# Infrastructure

Infrastructure repository containing all the deployment and automation.

# Repository contents
## Docs
Directory with all the laTex / README documentation.
## Terraform
Directory with all the terraform scripts.
## Ansible
Directory with all the Ansible scripts.
## Python
Directory with all the python scripts.
